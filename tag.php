<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="row"  data-equalizer>
<!-- Row for main content area -->
	<div class="" role="main">
		
	<?php if ( have_posts() ) : ?>

		<?php /* Start the Loop */ ?>
		<?php while ( have_posts() ) : the_post(); ?>
					
			<div class="columns large-6 medium-6 small-12 end excerpt" data-equalizer-watch>
		             <?php if ( has_post_thumbnail() ) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<span class="thumb">
								<?php the_post_thumbnail(); ?>
							</span>
						</a>
					<?php endif; ?>
					
					<span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span><br />
					<span class="file-under"><?php the_tags( 'File under: ', ', ', '' ); ?> </span><br />
					<span class="right more-info"><a href="<?php the_permalink(); ?>">More info</a></span>
					
					<?php $post_object = get_field('director'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
					<span class="director">Dir. <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
					<?php wp_reset_postdata(); ?><?php endif; ?><br />
					<div class="hr-blk large-12"></div>
		        </div>
		        
		<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>

	<?php endif; // End have_posts() check. ?>

	<?php /* Display navigation to next/previous pages when applicable */ ?>
	<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
		</nav>
	<?php } ?>

	</div>
	
</div>
<?php get_footer(); ?>
