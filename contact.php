<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>


<div class="row">
	
	<?php do_action( 'foundationpress_before_content' ); ?>

	<?php while ( have_posts() ) : the_post(); ?>
	
	<?php if( have_rows('contacts') ): ?>

   <?php while ( have_rows('contacts') ) : the_row(); ?>

        <div class="columns large-4 medium-4 small-12 end">
			<?php the_sub_field('copy'); ?>
        </div>

    <?php endwhile; ?>

<?php else : ?>

<?php endif; ?>
	
	
		
	<?php endwhile;?>

	<?php do_action( 'foundationpress_after_content' ); ?>

	</div>
	
</div>
<?php get_footer(); ?>
