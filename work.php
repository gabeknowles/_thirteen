<?php
/*
Template Name: Work
*/
?>
<?php get_header(); ?>

<div class="row">
	<?php do_action( 'foundationpress_before_content' ); ?>

	<?php while ( have_posts() ) : the_post(); ?>
	
		<div class="column">
			<?php if( have_rows('slideshow') ): ?>
				<ul class="" data-orbit data-options="bullets:false; slide_number: false; timer: true; timer_speed: 5000; animation_speed: 800;">
				  <?php while( have_rows('slideshow') ): the_row(); 
						$image = get_sub_field('image');
						$link = get_sub_field('link');
						?>
						<li>	
							<a href="<?php echo $link; ?>">
								<img src="<?php echo $image; ?>" />
							</a>
						</li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>
</div>

		
		    <div class="row" data-equalizer>
			<?php $post_objects = get_field('hero_spots'); if( $post_objects ): ?>
		    <?php foreach( $post_objects as $post): ?>
		        <?php setup_postdata($post); ?>
		        <div class="columns large-6 medium-6 small-12 end excerpt" data-equalizer-watch>
		             <?php if ( has_post_thumbnail() ) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<span class="thumb">
								<?php the_post_thumbnail(); ?>
							</span>
						</a>
					<?php endif; ?>
					
					<span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span><br />
					<span class="file-under"><?php the_tags( 'File under: ', ', ', '' ); ?> </span><br />
					<span class="right more-info"><a href="<?php the_permalink(); ?>">More info</a></span>
					
					<?php $post_object = get_field('director'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
					<span class="director">Dir. <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
					<?php wp_reset_postdata(); ?><?php endif; ?><br />
					<div class="hr-blk large-12"></div>
		        </div>
		    <?php endforeach; ?>
		    
		    </div>
		    <?php wp_reset_postdata(); ?>
		<?php endif;?>
		
		<?php endwhile;?>

	<?php do_action( 'foundationpress_after_content' ); ?>

	</div>
	 
</div>
<?php get_footer(); ?>
