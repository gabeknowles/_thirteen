<?php
/*
YARPP Template: Thumbnails
Description: Requires a theme which supports post thumbnails
Author: mitcho (Michael Yoshitaka Erlewine)
*/ ?>
<?php if (have_posts()):?>

	<?php while (have_posts()) : the_post(); ?>
		
		<div class="columns large-6 medium-6 small-12 end excerpt">
		           <?php if ( has_post_thumbnail() ) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<span class="thumb">
								<?php the_post_thumbnail(); ?>
							</span>
						</a>
					<?php endif; ?>
					
					<span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span><br />
					<span class="file-under"><?php the_tags( 'File under: ', ', ', '' ); ?> </span><br />
					<span class="right more-info"><a href="<?php the_permalink(); ?>">More info</a></span>
					
					<?php $post_object = get_field('director'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
					<span class="director">Dir. <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
					<?php wp_reset_postdata(); ?><?php endif; ?><br />
					<div class="hr-blk large-12"></div>
		        
		    
		</div>
		
	<?php endwhile; ?>


<?php else: ?>
<div class="columns large-6 medium-6 small-12 end excerpt">
	<p></p>
</div>
<?php endif; ?>
