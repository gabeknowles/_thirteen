<?php
/*
Template Name: Reel
*/
?>
<?php get_header(); ?>

<?php global $post;
$slug = get_post( $post )->post_name; ?>
<div class="row" data-equalizer>
	<div class="column large-12 password">
		<?php if ( post_password_required() ) { echo get_the_password_form(); } else { ?>
	</div>
	<?php do_action( 'foundationpress_before_content' ); ?>
	
	<?php while ( have_posts() ) : the_post(); ?>
	<?php $post_objects = get_field('spots'); if( $post_objects ): ?>
	<?php foreach( $post_objects as $post): ?>
	<?php setup_postdata($post); ?>
	<article <?php post_class( 'column large-6 medium-6 small-12 spot' ) ?> id="post-<?php the_ID(); ?>">
			<header>
				<!--<h1 class="entry-title"><?php the_title(); ?></h1>-->
			</header>
			<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
			<div class="entry-content">
				<?php //the_content(); ?>
				<div class="player">
				<iframe id="player1" src="https://player.vimeo.com/video/<?php the_field('vimeo'); ?>?api=1&player_id=player1" width="630" height="354" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
					<!--<div>
					  <button id="play-button">Play</button>
					<button id="pause-button">Pause</button>
					</div>-->
				<h1><?php the_title(); ?></h1>
				<?php $post_object = get_field('director'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
					<span class="director">Dir. <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
				<?php wp_reset_postdata(); ?><?php endif; ?><br/>
				<?php if( get_field('client_link') ): ?>
					<span class="client"><a href="<?php the_field('client_link'); ?>" target="_blank">
				<?php endif; ?>
				<?php if( get_field('client_name') ): ?>
					Client. <?php the_field('client_name'); ?>
				<?php endif; ?>
				<?php if( get_field('client_link') ): ?>
					</a></span>
				<?php endif; ?>
			</div>
			<footer>
				<?php //wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php //the_tags(); ?></p>
			</footer>
			
		</article>
	<?php endforeach; ?>
	
	</div>
	<?php wp_reset_postdata(); ?>
	<?php endif;?>
	
	
	<div class="row">
	<?php if ( get_field( 'show_director' ) ): ?>
	<div class="column large-12 medium-12 small-12 director-bio">
	<?php $post_object = get_field('director'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
	
	<?php $image = get_field('director_image');
	if( !empty($image) ): ?>
	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>" />
	<?php endif; ?>
	<h1><?php the_title(); ?></h1>
	<?php the_field('director_bio'); ?>
	
	<?php wp_reset_postdata(); ?>
	<?php endif;?>
	</div>
	<?php else: ?>
	
	<?php endif;?>
	</div>
	
	
	<?php endwhile;?>
	
	<?php do_action( 'foundationpress_after_content' ); ?>
	
	</div>
	<?php } ?>
</div>

<?php get_footer(); ?>