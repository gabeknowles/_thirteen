<?php
/*
Template Name: About
*/
?>
<?php get_header(); ?>

<div class="row">
	
	<?php do_action( 'foundationpress_before_content' ); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<div class="about-image columns large-12 medium-12 small-12">
			<?php $image = get_field('hero_image');
				if( !empty($image) ): ?>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>" />
			<?php endif; ?>
		</div>
		<div class="clear"></div>
		<article <?php post_class( 'columns large-6 medium-6 small-12' ) ?> id="post-<?php the_ID(); ?>">
			<!--<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>-->
			<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
			<div class="entry-content">
				<?php the_content(); ?>
				<?php the_field('copy'); ?>
			</div>
			
			<!-- Begin MailChimp Signup Form -->
			<div id="mc_embed_signup" class="row">
			<form action="//13co.us12.list-manage.com/subscribe/post?u=f1df859b79a7fafecd56d7ef2&amp;id=51961487aa" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			    <div id="mc_embed_signup_scroll">
				
			<div class="mc-field-group columns large-6 medium-6 small-6">
				<!--<label for="mce-EMAIL">Email Address </label>-->
				<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Newsletter">
			</div>
				<div id="mce-responses" class="clear">
					<div class="response" id="mce-error-response" style="display:none"></div>
					<div class="response" id="mce-success-response" style="display:none"></div>
				</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
			    <div style="position: absolute; left: -5000px;"><input type="text" name="b_f1df859b79a7fafecd56d7ef2_51961487aa" tabindex="-1" value=""></div>
			    <div class="columns large-3 medium-3 small-3"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
			    </div>
			</form>
			</div>
			
			<!--End mc_embed_signup-->
			
			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php the_tags(); ?></p>
			</footer>
			
		</article>
	<?php endwhile;?>

	<?php do_action( 'foundationpress_after_content' ); ?>

	</div>
	
</div>
<?php get_footer(); ?>
