<?php
/**
 * Template part for top bar menu
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<div class="top-top-bar row" data-equalizer data-equalizer-mq="large-up">
	<div class="logo columns large-6 medium-6 small-12" data-equalizer-watch>
		<!--<a href="<?php echo home_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/13CO-LOGO.jpg"></a>-->
		<a href="<?php echo home_url(); ?>"><span id="logo"></span></a>
	</div>
	<div class="social columns large-6 medium-6 small-12 show-for-large-up" data-equalizer-watch>
		<div class="icons right">
			<ul class="inline-list">
		<li class="facebook">
                <a href="https://www.facebook.com/13Company" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/facebook.svg"></a>
        </li>
        <li class="instagram">
                <a href="https://instagram.com/13co_collective/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/instagram.svg"></a>
        </li>
        <li class="vimeo">
                <a href="https://vimeo.com/13co" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/vimeo.svg"></a>
        </li>
			</ul>
		</div>
	</div>
</div>


<div class="row">	
	<div class="top-bar-container column small-6 medium-6 large-6 small-right medium-right">
	    <nav class="top-bar" data-topbar role="navigation">
	        <ul class="title-area top-bar-<?php echo apply_filters( 'filter_mobile_nav_position', 'mobile_nav_position' ); ?>">
	            <!--<li class="name">
	                <h1><a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a></h1>
	            </li>-->
	            <li class="toggle-topbar menu-icon"><a href="#"><li></li></a></li>
	        </ul>
	        <section class="top-bar-section show-for-large-up">
	            <?php foundationpress_top_bar_l(); ?>
	            <?php //foundationpress_top_bar_r(); ?>
	        </section>
	    </nav>
	</div>
</div>

<div class="row">	
    <div class="directors-menu column show-for-large-up large-6 medium-6 end"  data-equalizer>
	    <div class="directors columns large-2 no-pad-left"  data-equalizer-watch>
		    <span>Directors: <?php echo $_GET['reel']; ?></span>
	    </div>
	    <div class="menu columns large-10 medium-6"  data-equalizer-watch>
		    <section class="top-bar-section">
	    	<?php foundationpress_top_bar_r(); ?>
		    </section>
	    </div>
	    <div class="hr-blk show-for-large-up"></div>
    </div>
    
    
    
</div>
