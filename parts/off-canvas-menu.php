<?php
/**
 * Template part for off canvas menu
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<nav class="tab-bar hide-for-large-up">
  <section class="<?php echo apply_filters( 'filter_mobile_nav_position', 'mobile_nav_position' ); ?>-small">
    <a class="<?php echo apply_filters( 'filter_mobile_nav_position', 'mobile_nav_position' ); ?>-off-canvas-toggle menu-icon" href="#"><span></span></a>
  </section>
  <!--<section class="middle tab-bar-section">

    <h1 class="title">
      <?php bloginfo( 'name' ); ?>
    </h1>

  </section>-->
</nav>

<aside class="<?php echo apply_filters( 'filter_mobile_nav_position', 'mobile_nav_position' ); ?>-off-canvas-menu" aria-hidden="true">
    <?php //foundationpress_mobile_off_canvas( apply_filters('filter_mobile_nav_position', 'mobile_nav_position') ); ?>
    <?php foundationpress_top_bar_l(); ?>
    <ul class="directors">
	    <li>Directors:</li>
    </ul>
    <?php foundationpress_top_bar_r(); ?>
    <div class="social">
		<div class="icons">
			<ul class="inline-list">
		<li class="facebook">
                <a href="https://www.facebook.com/13Company" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/facebook.svg"></a>
        </li>
        <li class="instagram">
                <a href="https://instagram.com/13co_collective/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/instagram.svg"></a>
        </li>
        <li class="vimeo">
                <a href="https://vimeo.com/13co" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/vimeo.svg"></a>
        </li>
			</ul>
		</div>
	</div>
	
	<div class="social">
		<a href="mailto:hello@13co.io">hello@13co.io</a>&nbsp; &nbsp; &nbsp;©//13&Co
	</div>
</aside>


