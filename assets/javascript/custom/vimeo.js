$(function() {
    var iframe = $('#player1')[0];
    var player = $f(iframe);
    var status = $('.status');
    
    var playButton = document.getElementById("play-button");
	playButton.addEventListener("click", function() {
	  player.api("play");
	});

	var pauseButton = document.getElementById("pause-button");
	pauseButton.addEventListener("click", function() {
	  player.api("pause");
	});

   
});