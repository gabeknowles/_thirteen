<?php
/*
Template Name: Director
*/
?>
<?php get_header(); ?>

<div class="row" data-equalizer>
	<?php do_action( 'foundationpress_before_content' ); ?>

	<?php while ( have_posts() ) : the_post(); ?>
			<?php $post_objects = get_field('spots'); if( $post_objects ): ?>
		    <?php foreach( $post_objects as $post): ?>
		        <?php setup_postdata($post); ?>
		        <div class="columns large-6 medium-6 small-12 end excerpt" data-equalizer-watch>
		           <?php if ( has_post_thumbnail() ) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<span class="thumb">
								<?php the_post_thumbnail(); ?>
							</span>
						</a>
					<?php endif; ?>
					
					<span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span><br />
					<span class="file-under"><?php the_tags( 'File under: ', ', ', '' ); ?> </span><br />
					<span class="right more-info"><a href="<?php the_permalink(); ?>">More info</a></span>
					
					<?php $post_object = get_field('director'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
					<span class="director">Dir. <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
					<?php wp_reset_postdata(); ?><?php endif; ?><br />
					<div class="hr-blk large-12"></div>
		        </div>
		    <?php endforeach; ?>
		    
		    </div>
		    <?php wp_reset_postdata(); ?>
		<?php endif;?>
		
		<div class="row">
			<div class="column large-12 medium-12 small-12 director-bio">
				<?php $image = get_field('director_image');
					if( !empty($image) ): ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>" />
				<?php endif; ?>
				<h1><?php the_title(); ?></h1>
				<?php the_field('director_bio'); ?>
			</div>
		</div>
		
	<?php endwhile;?>

	<?php do_action( 'foundationpress_after_content' ); ?>

	</div>
</div>
<?php get_footer(); ?>
